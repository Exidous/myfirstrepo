﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" Then
            'call the PeCompact unpacker (The File To Unpack) 
            UnpackPeCompact(TextBox1.Text)
        End If
        'After unpacking end the program (loader)
        End
    End Sub

    Private Sub UnpackPeCompact(ByVal TheFile As String)
        'y is a placeholder for diffrent Long values
        Dim y As Long
        'NextBP is a placeholder for the next breakpoint
        Dim NextBp As Long
        'Create a Debugger instance of the nonintrusive debugger
        Dim Debugger As New NonIntrusive.NIDebugger
        'Create a Process Object (I just called it a ProcessHandle)
        Dim ProcessHandle As New Process
        'opts are Startup Options for the nonintrusive debugger
        Dim opts As New NonIntrusive.NIStartupOptions()
        'Tell the debugger which file to debug
        opts.executable = TheFile
        'Tell the debugger to not resume thread after creating the process
        opts.resumeOnCreate = False
        'Tell the debugger what to do with the file.
        With Debugger
            'Auto clear breakpoints after being hit
            .AutoClearBP = True
            'Execute the program and keep a process object (So we can kill it later)
            ProcessHandle = .Execute(opts)
            'Get the adress to the first instruction after EP
            y = .getDword(.Context.Eip + 1)
            'Set a breakpoint on the adress to first instruction after EP
            .setBreakpoint(y)
            'Run to breakpoint
            .Continue()
            'Single step 3 times (like F7 in olly)
            .SingleStep(3)
            'Get the value of ecx, that is where we need to breakpoint next
            NextBp = .Context.Ecx
            'Set breakpoint (ECX)
            .setBreakpoint(NextBp)
            'Run to breakpoint
            .Continue()
            'Find the Jump EAX (Opcode FFE0)
            Dim OpCode As String = "FF E0"
            'Empty variable we will be using to see if the current opcode is Jump EAX
            Dim z As String = ""
            'Tell the debugger not to step into calls when single stepping
            .StepIntoCalls = False
            'loop until the current opcode = Jump EAX
            Do Until z = OpCode
                'Step one time
                .SingleStep()
                'Get the current opcode we are paused at
                z = .getInstrOpcodes()
                'check to see if the opcode we are at = Jump EAX
                If InStr(z, OpCode) Then
                    'If we are at jump EAX then exit the loop
                    Exit Do
                End If
                'keep trying to find Jump EAX
            Loop
            'Now that we are at the JMP EAX we will set a BreakPoint on OEP (EAX)
            .setBreakpoint(.Context.Eax)
            'Run until hit BreakPoint
            .Continue()
            'Show the Adress to OEP in textbox1
            TextBox1.Text = Hex(.Context.Eax)

            .setBreakpoint(.Context.Eax)

            .Continue()
            'h is a placeholder for the OEP/RVA 
            Dim h As Long
            'Set H to OEP/RVA
            h = .Context.Eip - ProcessHandle.Modules(0).BaseAddress
            'clear the clipboard
            Clipboard.Clear()
            'set clipboard OEP/RVA
            Clipboard.SetText(Hex(h))
            'Dump the process to disk
            Dim DmpOpts As New NonIntrusive.DumpOptions
            DmpOpts.ChangeEP = True
            DmpOpts.EntryPoint = h
            Dim s(0 To 1) As String
            s(0) = Strings.Left(TheFile, TheFile.LastIndexOf("\")) & "\"
            s(1) = Strings.Right(TheFile, TheFile.Length - TheFile.LastIndexOf("\") - 1)
            DmpOpts.OutputPath = s(0) & "Dump_" & s(1)
            DmpOpts.PerformDumpFix = True
            .DumpProcess(DmpOpts)
            'Show the user that we are now at OEP and need to use ImportREC or ChimpREC to fix Imports
            MsgBox("Fix Imports!" & vbCrLf & "OEP/RVA Saved To Clipboard!")
            'Detach debugger from target
            .Detach()
            'kill the target
            ProcessHandle.Kill()
            'Done unpacking
        End With


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        With OpenFileDialog1
            .Title = "Select Program To Unpack"
            .FileName = ""
            .ShowDialog()
        End With

        If OpenFileDialog1.FileName <> "" Then
            TextBox1.Text = OpenFileDialog1.FileName
        End If
    End Sub
End Class
